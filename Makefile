SHELL = /bin/bash

srcdir = .
top_srcdir = .

prefix = /usr/local
exec_prefix = ${prefix}

bindir = ${exec_prefix}/bin
datadir = ${prefix}/share
sysconfdir = ${prefix}/etc
localstatedir = ${prefix}/var
libdir = ${exec_prefix}/lib
infodir = ${prefix}/share/info
mandir = ${prefix}/share/man
includedir = ${prefix}/include
oldincludedir = /usr/include

pkgdatadir = $(datadir)/sdldoom
pkglibdir = $(libdir)/sdldoom
pkgincludedir = $(includedir)/sdldoom

top_builddir = .

INSTALL = /usr/bin/install -c
INSTALL_DATA = ${INSTALL} -m 644

CC = gcc
PACKAGE = sdldoom
SDL_CFLAGS = -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT
SDL_CONFIG = /usr/bin/sdl-config
# XXX
SDL_LIBS = -L/usr/lib/x86_64-linux-gnu -lSDL
VERSION = 1.11

EXTRA_DIST = Changelog DOOMLIC.TXT FILES README README-orig README-gl TODO-orig
doom_SOURCES = am_map.c am_map.h d_englsh.h d_event.h d_items.c d_items.h d_main.c d_main.h d_net.c d_net.h \
	       d_player.h d_textur.h d_think.h d_ticcmd.h doomdata.h doomdef.c doomdef.h doomstat.c doomstat.h \
	       doomtype.h dstrings.c dstrings.h f_finale.c f_finale.h f_wipe.c f_wipe.h g_game.c g_game.h hu_lib.c \
	       hu_lib.h hu_stuff.c hu_stuff.h i_main.c i_net.c i_net.h i_sound.c i_sound.h i_system.c i_system.h \
	       i_video.c i_video.h info.c info.h m_argv.c m_argv.h m_bbox.c m_bbox.h m_cheat.c m_cheat.h m_fixed.c \
	       m_fixed.h m_menu.c m_menu.h m_misc.c m_misc.h m_random.c m_random.h m_swap.c m_swap.h p_ceilng.c \
	       p_doors.c p_enemy.c p_floor.c p_inter.c p_inter.h p_lights.c p_local.h p_map.c p_maputl.c p_mobj.c \
	       p_mobj.h p_plats.c p_pspr.c p_pspr.h p_saveg.c p_saveg.h p_setup.c p_setup.h p_sight.c p_spec.c \
	       p_spec.h p_switch.c p_telept.c p_tick.c p_tick.h p_user.c r_bsp.c r_bsp.h r_data.c r_data.h \
	       r_defs.h r_draw.c r_draw.h r_local.h r_main.c r_main.h r_plane.c r_plane.h r_segs.c r_segs.h \
	       r_sky.c r_sky.h r_state.h r_things.c r_things.h s_sound.c s_sound.h sounds.c sounds.h st_lib.c \
	       st_lib.h st_stuff.c st_stuff.h tables.c tables.h v_video.c v_video.h w_wad.c w_wad.h wi_stuff.c \
	       wi_stuff.h z_zone.c z_zone.h

PROGRAMS = doom 
doom_LDADD = -lm

#DEFS = -I$(srcdir)

LDFLAGS = 
LIBS = -L/usr/lib32 -L/usr/lib -L/usr/lib/i386-linux-gnu/ -lSDL

doom_OBJECTS =  am_map.o d_items.o d_main.o d_net.o doomdef.o doomstat.o \
		dstrings.o f_finale.o f_wipe.o g_game.o hu_lib.o hu_stuff.o i_main.o \
		i_net.o i_sound.o i_system.o i_video.o info.o m_argv.o m_bbox.o \
		m_cheat.o m_fixed.o m_menu.o m_misc.o m_random.o m_swap.o p_ceilng.o \
		p_doors.o p_enemy.o p_floor.o p_inter.o p_lights.o p_map.o p_maputl.o \
		p_mobj.o p_plats.o p_pspr.o p_saveg.o p_setup.o p_sight.o p_spec.o \
		p_switch.o p_telept.o p_tick.o p_user.o r_bsp.o r_data.o r_draw.o \
		r_main.o r_plane.o r_segs.o r_sky.o r_things.o s_sound.o sounds.o \
		st_lib.o st_stuff.o tables.o v_video.o w_wad.o wi_stuff.o z_zone.o

doom_DEPENDENCIES = 
doom_LDFLAGS = 

CFLAGS = -O2 -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -m32 -march=i386

COMPILE = $(CC) $(DEFS) $(INCLUDES) $(CFLAGS)
CCLD = $(CC)
LINK = $(CCLD) $(CFLAGS) $(LDFLAGS) -o $@
DIST_COMMON =  TODO
DISTFILES = $(DIST_COMMON) $(SOURCES) $(HEADERS) $(EXTRA_DIST)

SOURCES = $(doom_SOURCES)
OBJECTS = $(doom_OBJECTS)

all: doom

.SUFFIXES:
.SUFFIXES: .c .o

.c.o:
	$(COMPILE) -c $<

doom: $(doom_OBJECTS) $(doom_DEPENDENCIES)
	@rm -f doom
	$(LINK) $(doom_LDFLAGS) $(doom_OBJECTS) $(doom_LDADD) $(LIBS)

tags: TAGS
TAGS:  $(HEADERS) $(SOURCES)  $(TAGS_DEPENDENCIES) $(LISP)
	tags=; \
	here=`pwd`; \
	list='$(SOURCES) $(HEADERS)'; \
	unique=`for i in $$list; do echo $$i; done | \
	  awk '    { files[$$0] = 1; } \
	       END { for (i in files) print i; }'`; \
	test -z "$(ETAGS_ARGS)$$unique$(LISP)$$tags" \
	  || (cd $(srcdir) && etags $(ETAGS_ARGS) $$tags  $$unique $(LISP) -o $$here/TAGS)

clean:
	@rm -f *.o $(PROGRAMS) TAGS

.PHONY: all clean
